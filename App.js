import { AppLoading } from "expo";
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from "react";
import { 
  Image,
  View,
  StyleSheet, 
} from 'react-native';

import Styles from './src/styles/constants';
import RoomList from './src/components/RoomList';
import Toast from './src/components/Toast';
import * as R from 'ramda';

export default function App() {
  const [isReady, setIsReady] = useState(false);
  const [toastMessage, setToastMessage] = useState(null);
  
  const _cacheImages = (images) => {
    return images.map(image => {
      if (typeof image === 'string') {
        return Image.prefetch(image);
      } else {
        return Asset.fromModule(image).downloadAsync();
      }
    });
  }

  const _cacheFonts = (fonts) => {
    return fonts.map(font => Font.loadAsync(font));
  }
  const _loadAssetsAsync = async() => {
    const fontAssets = _cacheFonts([{
      'roboto-light': require('./src/assets/fonts/Roboto-Light.ttf'),
      'roboto-regular': require('./src/assets/fonts/Roboto-Regular.ttf'),
      'roboto-bold': require('./src/assets/fonts/Roboto-Medium.ttf'),
    }
    ]);

    await Promise.all([...fontAssets]);
  }


  const _showToastMessage = async (_message) => {
    setToastMessage(_message);
  }

  const _renderToast = () => {
    if (R.isNil(toastMessage)) {
      return ( <View></View> );
    } else {
      return (
        <Toast message={toastMessage} showToastMessage={_showToastMessage} />
      );
    }
  }

  if (!isReady) {
    return (
        <AppLoading
            startAsync={_loadAssetsAsync}
            onFinish={() => {
                setIsReady(true);
            }}
            onError={err => {
              setIsReady(true);
              console.log(err);
              showToastMessage(err.message);
            }}
            autoHideSplash={true}
        />
    );
  } else {
    return (
      <View style={styles.body}>
        <RoomList showToastMessage={_showToastMessage}/>
        {_renderToast()}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: Styles.Color.background,
    color: Styles.Color.background.grey,
    fontFamily: 'roboto-regular',
    fontSize: 14,
  },
});
