// 
// Context for rooms detail
// 

import React from 'react'

const RoomsContext = React.createContext({});

export const RoomsProvider = RoomsContext.Provider;
export const RoomsConsumer = RoomsContext.Consumer;
export default RoomsContext;