// 
// Styles common settings
// 

import {Dimensions, Platform} from 'react-native';

const platform = Platform.OS;
const width = Dimensions
    .get('window')
    .width;
const height = Dimensions
    .get('window')
    .height;
const padding = 20;

export default {
    Color : {
        background: '#f5f5f5',
        grey: '#aaa',
        lightGrey: '#ddd',
        blue: '#2192fc',
        darkBlue: '#396ddd',
        white: '#fff',
        black: '#000'
    },
    Platform: platform,
    AppWidth : width,
    AppHeight : height,
    Padding : padding,
    RoomIconSize : 40,
    RoomCardWidth : (width - padding * 3) / 2,
    RoomCardHeight : 150,
    FixtureIconSize : 36,
    FixtureCardWidth : (width - padding * 5) / 2,
    FixtureCardHeight : 180,
    AnimationDuration : 300
};