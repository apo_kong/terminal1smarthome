// 
// APIs common settings
// 


export default {
    Url : 'http://private-1e863-house4591.apiary-mock.com',
    Paths : {
        AllRooms: 'rooms', // Api path for getting room list
    },
    Storage : {
        ROOMS: 'ROOMS', // key for local storage
    },
    Weather : {
        Url: 'https://www.metaweather.com/api/location/2165352/',
        Delay: 30000, // ping every 0.5 min
    }
};