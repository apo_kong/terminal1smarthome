// 
// helper class to store global pure functions.
// 

export default {
    deepIndex : (p, o) => p.reduce((xs, x) => (xs && xs[x])
        ? xs[x]
        : null, o), // safely access deeply nested values
    parseApiPath : (n) => n
        .toString()
        .trim()
        .replace(' ', '-')
        .toLowerCase() // Parse key strings from Rooms Api response into pattern fit for on/off Api
};