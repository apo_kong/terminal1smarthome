// 
// useFetchTemperature hook to ping weather Api, delay time is configurable with default 10 sec.
// 


import {useState, useEffect} from "react";
import Helper from './helper';

const defaultDelay = 10000; // 10 sec

export default function useFetchTemperature(url, delay = defaultDelay) {
    const [error,
        setError] = useState(null);
    const [loading,
        setLoading] = useState(true);
    const [temp,
        setTemp] = useState(null);

    let weatherInterval = null;

    const _fetch = async() => {
        setLoading(true);
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const temp = Helper.deepIndex([
                    'consolidated_weather', 0, 'the_temp'
                ], data);
                setTemp(temp);
            } else {
                setError(new Error(response.statusText));
            }
        } catch (err) {
            setError(err);
        }
        setLoading(false);
    };

    useEffect(() => {
        console.disableYellowBox = true;
        _fetch();
        weatherInterval = setInterval(() => {
            _fetch();
        }, delay);
        return () => clearInterval(weatherInterval);
    }, [url]);
    return {error, loading, temp};
}