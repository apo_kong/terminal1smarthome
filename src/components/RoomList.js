// 
// RoomList component to list room cards
// 

import React, {useEffect, useState, useRef} from "react";
import {
    AsyncStorage,
    SafeAreaView,
    FlatList,
    StyleSheet,
    View,
    Text
} from 'react-native';
import Apis from '../apis/constants';
import useFetchTemperature from '../apis/useFetchTemperature';
import Helper from '../apis/helper';
import {RoomsProvider} from '../contexts/RoomsContext'

import HomeHeader from './HomeHeader';
import RoomCard from './RoomCard';
import ExpandedRoomCard from './ExpandedRoomCard';
import Styles from '../styles/constants';
import * as R from 'ramda';

export default function RoomList(props) {
    const container = useRef(null);
    const [roomList,
        setRoomList] = useState(null);
    const [yOffset,
        setYOffset] = useState(null);
    const [xOffset,
        setXOffset] = useState(null);
    const [selectedRoomIndex,
        setSelectedRoomIndex] = useState(null);
    const weatherApi = useFetchTemperature(Apis.Weather.Url, Apis.Weather.Delay);

    const _renderContent = () => {
        if (R.isEmpty(roomList)) {
            return (
                <Text style={styles.title}>
                    Loading Rooms...
                </Text>
            );
        } else {
            return (
                <FlatList
                    style={{
                    flex: 1
                }}
                    columnWrapperStyle={{
                    justifyContent: 'space-between'
                }}
                    contentContainerStyle={styles.roomlist}
                    horizontal={false}
                    numColumns={2}
                    data={roomList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => <RoomCard
                    hasShadow={true}
                    room={item}
                    roomIndex={index}
                    selectRoom={(xOffset, yOffset, index) => {
                    container.current.measure((fx, fy, width, height, px, py) => {
                            setSelectedRoomIndex(index);
                            setXOffset(xOffset);
                            setYOffset(yOffset - py);
                        });
                }}/>}
                    scrollEventThrottle={16}/>
            );
        }
    };

    const _renderExpandedCard = () => {
        return !R.isNil(selectedRoomIndex) && !R.isNil(yOffset) && !R.isNil(xOffset) && ( <ExpandedRoomCard
            yOffset={yOffset}
            xOffset={xOffset}
            roomIndex={selectedRoomIndex}
            room={roomList[selectedRoomIndex]}
            weatherApi={weatherApi}
            unselectRoom={() => {
                setSelectedRoomIndex(null);
                setXOffset(null);
                setYOffset(null);
            }}
            switchFixture={_sendFixtureApi}
            switchAutoAC={_switchAutoAC} />
        );
    };

    const _switchAutoAC = (roomIndex, value) => {
        const rooms = [...roomList],
            room = rooms[roomIndex];
        room.autoAC = value;
        setRoomsContext(rooms);
    };

    const _sendFixtureApi = (roomIndex, fixtureIndex, newState) => {
        const rooms = [...roomList],
            room = rooms[roomIndex],
            roomPath = Helper.parseApiPath(room.title),
            fixture = room.fixtures[fixtureIndex],
            fixturePath = Helper.parseApiPath(fixture.title),
            url = `${Apis.Url}/${roomPath}/${fixturePath}/${newState}`;
        fetch(url).then(response => {
            if (response.ok) {
                fixture.on = !fixture.on;
                setRoomsContext(rooms);
            } else {
                throw new Error(response.statusText);
            }
        }).catch(err => {
            console.log(err);
            props.showToastMessage(err.message);
        });
    };

    const setRoomsContext = async(_roomList) => {
        if (!R.isNil(_roomList)) {
            AsyncStorage.setItem(Apis.Storage.ROOMS, JSON.stringify(_roomList));
        }
        setRoomList(_roomList);
    }

    useEffect(() => {
        let cachedRooms = null;
        AsyncStorage
            .getItem(Apis.Storage.ROOMS)
            .then(value => {
                if (!R.isNil(value)) {
                    cachedRooms = JSON.parse(value);
                    setRoomsContext(cachedRooms);
                }
                return fetch(`${Apis.Url}/${Apis.Paths.AllRooms}`);
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.statusText);
                }
            })
            .then(data => {
                if (!data.rooms) {
                    throw new Error('Response has no rooms')
                } else {
                    const new_roomlist = Object
                        .keys(data.rooms)
                        .map((key) => {
                            let cachedroom = null;
                            if (!R.isNil(cachedRooms)) 
                                cachedroom = cachedRooms.find(r => r.title === key);
                            return {
                                title: key,
                                autoAC: !R.isNil(cachedroom) && (cachedroom.autoAC === true || R.isNil(cachedroom.autoAC)),
                                fixtures: data
                                    .rooms[key]
                                    .fixtures
                                    .map(fixture => {
                                        let cachedfixture = null;
                                        if (!R.isNil(cachedroom)) 
                                            cachedfixture = cachedroom.fixtures.find(r => r.title === fixture);
                                        return {
                                            title: fixture,
                                            on: (!R.isNil(cachedfixture) && cachedfixture.on === true)
                                        };
                                    })
                            }
                        });
                    cachedRooms = new_roomlist;
                    setRoomsContext(cachedRooms);
                }
            })
            .catch(err => {
                console.log(err);
                props.showToastMessage(err.message);
            });

    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container} ref={container} collapsable={false}>
                <RoomsProvider value={roomList}>
                    <HomeHeader/>
                    <Text style={styles.title}>
                        Rooms
                    </Text>
                    {_renderContent()}
                    {_renderExpandedCard()}
                </RoomsProvider>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        opacity: 1,
    },
    title: {
        fontFamily: 'roboto-bold',
        color: '#000',
        fontSize: 14,
        paddingTop: 10,
        paddingLeft: Styles.Padding,
        paddingRight: Styles.Padding
    },
    roomlist: {
        padding: Styles.Padding
    }
});
