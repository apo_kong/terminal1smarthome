// 
// HomeHeader component showing app title on landing page
// 


import React from "react";
import {StyleSheet, View, Text} from 'react-native';

import Divider from './Divider';
import Styles from '../styles/constants';

export default function HomeHeader() {
    return (
        <View style={styles.container}>
            <Text style={styles.header_title}>
                T1 Smart Home
            </Text>
            <Divider/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: Styles.Padding,
        paddingBottom: 10
    },
    header_title: {
        fontFamily: 'roboto-bold',
        color: '#000',
        fontSize: 24,
        paddingTop: 20
    }
});
