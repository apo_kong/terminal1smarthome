// 
// FixtureIcon component to display different icon based on fixture key
// 


import React from "react";
import IconSlowcooker from "../assets/images/slow-cooker.svg";
import IconLight from "../assets/images/lamp.svg";
import IconMusic from "../assets/images/cd.svg";
import IconAC from "../assets/images/airconditioner.svg";
import IconTV from "../assets/images/television.svg";

export default function FixtureIcon(props) {

    switch (props.type) {
        case 'AC':
            return (<IconAC {...props}/>);
        case 'Music':
            return (<IconMusic {...props}/>);
        case 'Slowcooker':
            return (<IconSlowcooker {...props}/>);
        case 'TV':
            return (<IconTV {...props}/>);
        default:
            return (<IconLight {...props}/>);
    }
}