// 
// Toast component to show error message
// 

import React, {useEffect, useState} from "react";
import {Animated, SafeAreaView, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Styles from '../styles/constants';

export default function Toast(props) {
    const [animatedValue,
        setAnimatedValue] = useState(new Animated.Value(0));

    const getTranslate = outputRange => {
        return animatedValue.interpolate({
            inputRange: [
                0, 1
            ],
            outputRange
        });
    };

    const _onPress = () => {
        Animated
            .timing(animatedValue, {
            toValue: 0,
            duration: Styles.AnimationDuration
        })
            .start(() => {
                props.showToastMessage(null);
            });
    };

    useEffect(() => {
        Animated
            .timing(animatedValue, {
            toValue: 1,
            duration: Styles.AnimationDuration
        })
            .start();
    }, [])

    return (
        <SafeAreaView style={styles.safearea}>
            <Animated.View
                style={[
                styles.container, {
                    transform: [
                        {
                            scaleY: getTranslate([0, 1])
                        }
                    ]
                }
            ]}>
                <TouchableOpacity
                    style={{
                    flex: 1
                }}
                    onPress={_onPress}>
                    <Text style={styles.message}>
                        {props.message}
                    </Text>
                </TouchableOpacity>
            </Animated.View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Styles.Color.black,
        opacity: .6,
        left: Styles.Padding,
        width: Styles.AppWidth - Styles.Padding * 2,
        color: Styles.Color.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        borderRadius: 10,
        padding: Styles.Padding
    },
    safearea: {
        flex: 1,
        position: 'absolute'
    },
    message: {
        fontFamily: 'roboto-bold',
        color: Styles.Color.white,
        fontSize: 14
    }
});
