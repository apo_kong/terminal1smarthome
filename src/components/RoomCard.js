// 
// RoomCard component showing room name and no. of fixtures
// 

import React, {useRef} from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import Styles from '../styles/constants';
import RoomIcon from './RoomIcon';

export default function RoomCard(props) {
    const container = useRef(null);

    const _onPress = () => {
        container.current.measure((fx, fy, width, height, px, py) => {
                if (props.selectRoom) 
                    props.selectRoom(px, py, props.roomIndex);
                }
            );
    };

    return (
        <View ref={container} style={{opacity: 1}} collapsable={false}>
            <TouchableOpacity activeOpacity={1} onPress={_onPress}>
                <View
                    style={[
                    styles.container,
                    props.hasShadow ? Styles.Platform === 'ios' ? {
                        shadowColor: '#ccc',
                        shadowOpacity: .3,
                        shadowOffset: {
                            width: 0,
                            height: 10
                        },
                        shadowRadius: 10
                    } : {
                        elevation: 2,
                        borderBottomWidth: 0,
                    } : {}
                ]}>
                    <RoomIcon
                        width={Styles.RoomIconSize}
                        height={Styles.RoomIconSize}
                        fill={Styles.Color.blue}
                        type={props.room.title}/>
                    <View>
                        <Text style={styles.title}>{props.room.title}</Text>
                        <Text style={styles.subtitle}>{props.room.fixtures.length} Fixtures</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        borderRadius: 10,
        padding: Styles.Padding,
        marginBottom: Styles.Padding,
        width: Styles.RoomCardWidth,
        height: Styles.RoomCardHeight,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'roboto-bold',
        fontSize: 18,
        color: 'black'
    },
    subtitle: {
        fontFamily: 'roboto-bold',
        fontSize: 12,
        color: Styles.Color.grey,
        marginTop: 4
    }
});
