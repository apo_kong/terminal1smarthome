// 
// ExpandedRoomCard component showing expended room card in fullscreen, listing fixture cards, and toggle for AC auto mode
// 



import React, {useState, useEffect} from "react";
import {
    Animated,
    FlatList,
    StyleSheet,
    Switch,
    Text,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import RoomCard from './RoomCard';
import FixtureCard from './FixtureCard';
import Divider from './Divider';
import Styles from '../styles/constants';
import IconClose from "../assets/images/cancel.svg";
import * as R from 'ramda';

export default function ExpandedRoomCard(props) {
    const [animatedValue,
        setAnimatedValue] = useState(new Animated.Value(0));

    const getTranslate = outputRange => {
        return animatedValue.interpolate({
            inputRange: [
                0, 1
            ],
            outputRange
        });
    };

    const _unselect = () => {
        Animated
            .timing(animatedValue, {
            toValue: 0,
            duration: Styles.AnimationDuration
        })
            .start(() => {
                props.unselectRoom();
            });
    };

    const _onAutoACChange = (value) => {
        if (props.switchAutoAC) 
            props.switchAutoAC(props.roomIndex, value);
        };
    
    const _renderAutoAC = () => {
        if (props.room.fixtures.find(f => {
            return f.title === 'AC'
        })) {
            return (
                <View style={styles.autoAC}>
                    <View style={styles.autoAC_label}>
                        <Text style={styles.autoAC_title}>Turn {props.room.autoAC
                                ? 'off'
                                : 'on'}
                            AC auto mode</Text>
                        <Text style={styles.autoAC_subtitle}>Auto turn on AC if temperature above 25°C,</Text>
                        <Text style={styles.autoAC_subtitle}>and off if below.</Text>
                    </View>
                    <Switch
                        style={{
                        transform: [
                            {
                                scaleX: .5
                            }, {
                                scaleY: .5
                            }
                        ]
                    }}
                        trackColor={{
                        false: Styles.Color.grey,
                        true: Styles.Color.darkBlue
                    }}
                        ios_backgroundColor={props.room.autoAC
                        ? Styles.Color.darkBlue
                        : Styles.Color.grey}
                        thumbColor={Styles.Color.white}
                        onValueChange={_onAutoACChange}
                        value={props.room.autoAC}/>
                </View>
            );
        } else {
            return (<View />);
        }
    };

    useEffect(() => {
        Animated
            .timing(animatedValue, {
            toValue: 1,
            duration: Styles.AnimationDuration
        })
            .start();
    }, [])

    return R.isNil(props.roomIndex) ? (<View></View>) : (
            <Animated.View
                style={{
                position: "absolute",
                top: getTranslate([props.yOffset, Styles.Padding]),
                left: getTranslate([props.xOffset, Styles.Padding]),
                right: getTranslate([
                    Styles.AppWidth - props.xOffset - Styles.RoomCardWidth,
                    Styles.Padding
                ]),
                bottom: getTranslate([
                    Styles.AppHeight - props.yOffset - Styles.RoomCardHeight,
                    Styles.Padding
                ]),
                backgroundColor: "#fff",
                borderRadius: 10
            }}>
                <TouchableWithoutFeedback onPress={_unselect}>
                    <Animated.View
                        style={{
                        position: "absolute",
                        top: Styles.Padding,
                        right: Styles.Padding,
                        zIndex: 100,
                        opacity: getTranslate([0, 1])
                    }}>
                        <IconClose width={24} height={24} fill={'black'}/>
                    </Animated.View>
                </TouchableWithoutFeedback>
                <RoomCard {...props}/>
                <Animated.View
                    style={{
                    flex: 1,
                    zIndex: 100,
                    marginTop: -Styles.Padding * 2,
                    opacity: getTranslate([0, 1])
                }}>
                    <View style={styles.divider}>
                        <Divider/>
                    </View>
                    {_renderAutoAC()}
                    <FlatList
                        style={{
                            flex: 1,
                        }}
                        columnWrapperStyle={{
                        justifyContent: 'space-between'
                    }}
                        contentContainerStyle={{
                        padding: Styles.Padding
                    }}
                        horizontal={false}
                        numColumns={2}
                        data={props.room.fixtures}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => <FixtureCard {...props} fixture={item} fixtureIndex={index}/>}
                        scrollEventThrottle={16} />
                </Animated.View>

            </Animated.View>
        );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        padding: Styles.Padding,
        marginBottom: Styles.Padding,
        width: Styles.FixtureCardWidth,
        height: Styles.FixtureCardHeight,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    divider: {
        marginLeft: Styles.Padding,
        marginRight: Styles.Padding
    },
    autoAC: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: Styles.Padding,
        paddingRight: Styles.Padding
    },
    autoAC_label: {
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    autoAC_title: {
        fontFamily: 'roboto-bold',
        fontSize: 12,
        color: Styles.Color.black,
        paddingBottom: 4,
    },
    autoAC_subtitle: {
        fontFamily: 'roboto-regular',
        fontSize: 10,
        color: Styles.Color.grey
    }
});