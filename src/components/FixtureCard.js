// 
// FixtureCard component showing fixture name, on/off, toggle on/off state, and current temperature on AC card
// 


import React, {useEffect, useState, useRef} from "react";
import {StyleSheet, Switch, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../styles/constants';
import FixtureIcon from './FixtureIcon';
import IconTemp from "../assets/images/thermometer.svg";
import {RoomsConsumer} from '../contexts/RoomsContext';
import * as R from 'ramda';

export default function FixtureCard(props) {
    const container = useRef(null);
    const [roomList,
        setRoomList] = useState(null);
    const [isLoading,
        setIsLoading] = useState(false);

    const _onPress = () => {
        const newState = (roomList[props.roomIndex].fixtures[props.fixtureIndex].on)
            ? 'off'
            : 'on';
        setIsLoading(true);
        if (props.fixture.title === 'AC' && props.switchAutoAC) {
            props.switchAutoAC(props.roomIndex, false);
        }
        props.switchFixture(props.roomIndex, props.fixtureIndex, newState);
    };

    const _renderStatus = (_roomList) => {
        if (props.fixture.title === 'AC') {
            const temp = parseInt(props.weatherApi.temp);
            return (
                <View style={styles.subtitle}>
                    <IconTemp
                    width={16}
                    height={16}
                    fill={_roomList[props.roomIndex].fixtures[props.fixtureIndex].on
                    ? Styles.Color.white
                    : Styles.Color.grey} />
                    <Text style={{
                        color: _roomList[props.roomIndex].fixtures[props.fixtureIndex].on
                            ? Styles.Color.white
                            : Styles.Color.grey}} >
                        <Text style={styles.temp}>{temp.toString()}</Text>°C
                    </Text>
                </View>
            )
        } else {
            return (
                <View style={styles.temp}></View>
            )
        }
    };

    useEffect(() => {
        if (R.isNil(roomList)) 
            return;
        setIsLoading(false);
        const currentState = roomList[props.roomIndex].fixtures[props.fixtureIndex].on;
        if (props.fixture.title === 'AC' && roomList[props.roomIndex].autoAC) {
            if (props.weatherApi.temp > 25 && !currentState) {
                props.switchFixture(props.roomIndex, props.fixtureIndex, 'on');
            } else if (props.weatherApi.temp < 25 && currentState) {
                props.switchFixture(props.roomIndex, props.fixtureIndex, 'off');
            }
        }
        return () => {};
    }, [props.weatherApi])

    return (
        <RoomsConsumer>
            {_roomList => {
                setRoomList(_roomList);
                const isOn = _roomList[props.roomIndex].fixtures[props.fixtureIndex].on;
                return (
                    <View ref={container} style={{opacity: 1}} collapsable={false}>
                        <TouchableOpacity activeOpacity={1} onPress={_onPress}>
                            <View
                                style={[
                                styles.container,
                                Styles.Platform === 'ios'? (isOn ? styles.containerOn_ios : styles.containerOff_ios) : (isOn ? styles.containerOn_android : styles.containerOff_android)
                            ]}>
                                <View style={{
                                    height: 100,
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}>
                                    <FixtureIcon
                                        width={Styles.FixtureIconSize}
                                        height={Styles.FixtureIconSize}
                                        fill={isOn
                                        ? Styles.Color.white
                                        : Styles.Color.blue}
                                        type={props.fixture.title} /> 
                                    {_renderStatus(_roomList)}
                                    <Text
                                        style={[
                                        styles.loading, {
                                            color: isOn
                                                ? Styles.Color.white
                                                : Styles.Color.blue
                                        }
                                    ]}>{isLoading
                                            ? 'Connecting...'
                                            : null}</Text>
                                </View>
                                <View style={styles.containerBottom}>
                                    <Text
                                        style={[
                                        styles.title, {
                                            color: isOn
                                                ? Styles.Color.white
                                                : Styles.Color.black
                                        }
                                    ]}>{props.fixture.title}</Text>

                                    <Switch
                                        style={{
                                        transform: [
                                            {
                                                scaleX: .5
                                            }, {
                                                scaleY: .5
                                            }
                                        ]
                                    }}
                                        pointerEvents="none"
                                        trackColor={{
                                        false: Styles.Color.grey,
                                        true: Styles.Color.darkBlue
                                    }}
                                        ios_backgroundColor={isOn
                                        ? Styles.Color.darkBlue
                                        : Styles.Color.grey}
                                        thumbColor={Styles.Color.white}
                                        value={isOn}/>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            }}
        </RoomsConsumer>
    );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        padding: Styles.Padding,
        marginBottom: Styles.Padding,
        width: Styles.FixtureCardWidth,
        height: Styles.FixtureCardHeight,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    containerOn_ios: {
        backgroundColor: Styles.Color.blue,
        shadowColor: Styles.Color.blue,
        shadowOpacity: .3,
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowRadius: 10
    },
    containerOn_android: {
        borderBottomWidth: 0,
        elevation: 2,
        backgroundColor: Styles.Color.blue,
    },
    containerOff_ios: {
        backgroundColor: Styles.Color.white,
        shadowColor: '#ccc',
        shadowOpacity: .3,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 10
    },
    containerOff_android: {
        borderBottomWidth: 0,
        elevation: 2,
        backgroundColor: Styles.Color.white,
    },
    containerTop: {
        height: 100,
        flexDirection: 'column',
        alignItems: 'center'
    },
    containerBottom: {
        height: 80,
        flexDirection: 'column',
        alignItems: 'center'
    },
    title: {
        fontFamily: 'roboto-bold',
        fontSize: 18,
        marginBottom: 4
    },
    subtitle: {
        fontFamily: 'roboto-light',
        fontSize: 12,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    temp: {
        fontFamily: 'roboto-regular',
        fontSize: 18
    },
    loading: {
        fontFamily: 'roboto-light',
        fontSize: 12
    }
});
