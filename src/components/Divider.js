// 
// Divider component to seperate header and content in a pretty way
// 


import React from "react";
import {StyleSheet, View} from 'react-native';
import Styles from '../styles/constants';

export default function Divider() {
    return (
        <View style={styles.container}></View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: 60,
        height: 4,
        backgroundColor: Styles.Color.lightGrey,
        borderRadius: 2,
        marginTop: 10,
        marginBottom: 10
    }
});
