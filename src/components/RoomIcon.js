// 
// RoomIcon component to display different icon based on room key
// 


import React from "react";

import IconBedroom from "../assets/images/canopy-bed.svg";
import IconLivingroom from "../assets/images/cabinet.svg";
import IconKitchen from "../assets/images/kitchen.svg";
import IconOther from "../assets/images/plant.svg";

export default function RoomIcon(props) {

    switch (props.type) {
        case 'Bedroom':
            return (<IconBedroom {...props}/>);
        case 'Living Room':
            return (<IconLivingroom {...props}/>);
        case 'Kitchen':
            return (<IconKitchen {...props}/>);
        default:
            return (<IconOther {...props}/>);
    }
}