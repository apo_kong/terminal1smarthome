## Mobile Developer Take Home Assessment
This project was developed for Technical Assessments on Terminal 1
[Assessment detail](https://hackmd.io/@terminal1/assessment-mobile-native)



#### Author

Apo Kong [feel free to contact by email](mailto:apokong@gmail.com).


---


## Solutions

Use react-native to build hybrid IoT smart home mobile app for Mary 
to control fictures of her rooms, with connection to backend infrastructure APIs [here](http://private-1e863-house4591.apiary-mock.com/)

Functionality:

1. Landing page listing room cards (showing number of fixtures in each room)
2. Click room card will open related fixtures list. (showing on/off state, and current temperature on AC card)
3. Click on fixture card will turn on/off related fixture (showing 'connecting...' message when connecting to backend)
4. Fixture card turns into blue when state is on, white when off.
5. Default fixture states default is off
6. Every 0.5 minutes, update current temperature data from [MetaWeather API for Hong Kong](https://www.metaweather.com/api/location/2165352/)
7. Added toggle button to turn on/off AC's auto mode
8. If auto mode is on and temperature above 25°C, turn on all AC fixtures in related room
9. If auto mode is on and temperature below 25°C, turn off all AC fixtures in related room
10. Once user manually click on AC card to turn on/off AC, auto mode will be turned off
11. Once rooms list retrived from backend. Room details and states is cached locally. 
12. App will get cached details everytime when launch, then get latest list from backend API of rooms.
13. If API call sucess, will overwrite local cached details.
14. If API call fail, will still show cached details.
15. Error toast message will be shown in top when Api call fail. Click toast will close it.
16. By impletmenting React Native library, this app is using MVC architectural pattern in mainly 'V' driven. Unidirectional data binding with Context to manage rooms details.


---


## Technique used

1. NodeJS version 8 or above [Download](https://nodejs.org/en/)
2. Npm version 6 or above
3. Expo [Detail](https://expo.io/)
```bash
npm install expo-cli --global 
```
4. macOS
5. Xcode 9 or above [Download](https://developer.apple.com/xcode/)
6. Java Development Kit (version 8)
7. Android Studio [Download](https://developer.android.com/studio)


---


## Platforms

iOS (manually setup in XCode)

1. iOS Deployment Target version 10 or below
2. iOS Base SDK version 12 or above
3. only support iPhone handset

Android (manually setup in Android Studio)

1. minSdkVersion version 23 or below
2. targetSdkVersion version 28 or above


---


### Local test with Expo

#### Clone this [repository](https://apo_kong@bitbucket.org/apo_kong/terminal1smarthome.git)
```bash
git clone https://apo_kong@bitbucket.org/apo_kong/terminal1smarthome.git

cd Terminal1SmartHome
npm start
```



#### Run on iPhone simulator

```bash
npm run ios
```
Local server will be auto start on browser[http://localhost:19002/](http://localhost:19002/).
Next, click **Run on iOS simulator** will open XCode simulator.



#### Run on Android emulator

Make sure default adb emulator is setup already with Android Studio, or adb device connected in USB debug mode.

```bash
npm run android
```
Local server will be auto start on browser[http://localhost:19002/](http://localhost:19002/).
Next, click **Run on Android device/emulator** will open adb device/emulator.



#### Build iPhone app 
```bash
expo build:ios
```
Login Apple Developer account and plugin available certificate.



#### Build Android apk 
```bash
expo build:android -t apk
```


#### Download iOS demo video
[Download video file](https://bitbucket.org/apo_kong/terminal1smarthome/raw/db0bdfa39302bc207afdb71272ce76a35946f685/demo/ios_demo.mov)

#### Download Android demo 
[Download unsigned apk files](https://expo.io/artifacts/d04ec669-bafb-446e-91d0-d36aaea4fc15)